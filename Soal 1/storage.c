#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024
#define CSV_DELIMITER ","
#define CSV_QUOTE_CHARACTER "\""

int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    const char* zipFilePath = "fifa-player-stats-database.zip";
    const char* extractDir = "extracted_files";

    // Mengeksekusi perintah unzip menggunakan fungsi system
    int unzipResult = system("unzip -o fifa-player-stats-database.zip -d extracted_files");

    // Periksa apakah ekstraksi berhasil atau tidak
    if (unzipResult != 0) {
        fprintf(stderr, "Gagal mengekstrak file %s\n", zipFilePath);
        return 1;
    }

    const char* csvFilePath = "extracted_files/FIFA23_official_data.csv";

    // Open the CSV file
    FILE* file = fopen(csvFilePath, "r");
    if (file == NULL) {
        fprintf(stderr, "Failed to open file %s\n", csvFilePath);
        return 1;
    }

    // Read the header (first line) and ignore it
    char line[MAX_LINE_LENGTH];
    fgets(line, MAX_LINE_LENGTH, file);
    
    printf("\n\n -----Pemain yang sesuai kriteria ----- \n\n");

    // Read player data and print those that meet the criteria
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        // Extract player data from the CSV line
        char* saveptr;
        char* token = strtok_r(line, CSV_DELIMITER, &saveptr);
        char* ID = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Name = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Age = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLPhoto = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Nationality = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLFlag = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Overall = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Potential = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Club = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLClublogo = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Value = token;
      
     

        // Remove newline character from tokens
        size_t tokenLength = strcspn(ID, "\n");
        ID[tokenLength] = '\0';

        tokenLength = strcspn(Name, "\n");
        Name[tokenLength] = '\0';

        tokenLength = strcspn(URLPhoto, "\n");
        URLPhoto[tokenLength] = '\0';

        tokenLength = strcspn(Nationality, "\n");
        Nationality[tokenLength] = '\0';

        tokenLength = strcspn(Club, "\n");
        Club[tokenLength] = '\0';

        // Remove double quotes from URLPhoto and Club
        char* quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);
        if (quotePos != NULL) {
            memmove(quotePos, quotePos + 1, strlen(quotePos));
            quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);
            if (quotePos != NULL)
                *quotePos = '\0';
        }

        quotePos = strchr(Club, CSV_QUOTE_CHARACTER[0]);
        if (quotePos != NULL) {
            memmove(quotePos, quotePos + 1, strlen(quotePos));
            quotePos = strchr(Club, CSV_QUOTE_CHARACTER[0]);
            if (quotePos != NULL)
                *quotePos = '\0';
        }

        // Check the criteria
        if (Age < 25 && Potential > 85 && strcmp(Club, "Manchester City") != 0) {
            // Print player data that meet the criteria
            printf("Name: %s\n", Name);
            printf("Age: %d\n", Age);
            printf("Club: %s\n", Club);
            printf("Nationality: %s\n", Nationality);
            printf("Potential: %d\n", Potential);
            printf("URL Photo: %s\n", URLPhoto);

            printf("\n");
        }
    }

    // Close the CSV file
    fclose(file);
    return 0;
}

