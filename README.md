# sisop-praktikum-modul-4-2023-HS-E08

Anggota kelompok :
- Ilham Insan Wafi 5025211255
- Dilla Wahdana 5025211060
- Muhammad Ahyun Irsyada 5025211251

# Soal1

**Pada soal ini, kita diminta untuk menyelesaikan beberapa hal, yaitu:**

**A. Mendownload dataset tentang pemain sepak bola dari Kaggle. Lalu ekstrak hasil download tersebut.**

```
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
```
Kode diatas merupakan pemanggilan perintah sistem menggunakan fungsi `system()`. 
Perintah sistem yang dijalankan adalah perintah dengan menggunakan utilitas `Kaggle` untuk mengunduh dataset dengan nama `fifa-player-stats-database` yang dibuat oleh pengguna bernama `bryanb`. 
`Kaggle` adalah platform yang menyediakan dataset, kompetisi, dan sumber daya terkait ilmu data dan pembelajaran mesin. Dengan menjalankan perintah tersebut, kode akan memulai proses pengunduhan dataset `fifa-player-stats-database` menggunakan utilitas `Kaggle`.

```
const char* zipFilePath = "fifa-player-stats-database.zip";
const char* extractDir = "extracted_files";
```
- Baris pertama diatas mendefinisikan variabel `zipFilePath` sebagai string yang berisi nama file ZIP yang akan diekstrak. Dalam program ini, file ZIP bernama `fifa-player-stats-database.zip`.
- Baris kedua diatas mendefinisikan variabel `extractDir` sebagai string yang berisi nama direktori tempat file ZIP akan diekstrak. Dalam program ini, direktori tersebut bernama `extracted_files`.

```
int unzipResult = system("unzip -o fifa-player-stats-database.zip -d extracted_files");
```
Kode diatas menggunakan fungsi `system()` untuk menjalankan perintah sistem `unzip`. Perintah ini mengarahkan sistem untuk mengekstrak file ZIP yang bernama `fifa-player-stats-database.zip` ke dalam direktori `extracted_files`.
Jika perintah berhasil dieksekusi, nilai `unzipResult` akan menjadi `0`. Jika tidak, nilai `unzipResult` akan menunjukkan kode kesalahan yang sesuai.

```
if (unzipResult != 0) {
        fprintf(stderr, "Gagal mengekstrak file %s\n", zipFilePath);
        return 1;
    }
```
Kode diatas memeriksa apakah ekstraksi file ZIP berhasil atau tidak. Jika `unzipResult` tidak sama dengan `0`, itu berarti ada kesalahan dalam proses ekstraksi. Dalam program ini, pesan kesalahan akan dicetak ke `stderr` menggunakan `fprintf()`, dan fungsi akan mengembalikan nilai `1`, menunjukkan kegagalan dalam ekstraksi file ZIP.

**B. Membaca file CSV khusus bernama `FIFA23_official_data.csv` dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya.** 

```
const char* csvFilePath = "extracted_files/FIFA23_official_data.csv";

    // Open the CSV file
    FILE* file = fopen(csvFilePath, "r");
    if (file == NULL) {
        fprintf(stderr, "Failed to open file %s\n", csvFilePath);
        return 1;
    }

    // Read the header (first line) and ignore it
    char line[MAX_LINE_LENGTH];
    fgets(line, MAX_LINE_LENGTH, file);
```
- Mendefinisikan variabel `csvFilePath` sebagai string yang berisi path atau jalur file CSV yang akan dibuka. Dalam program ini, path file CSV adalah `extracted_files/FIFA23_official_data.csv`.
- Menggunakan fungsi `fopen()` untuk membuka file CSV. Fungsi `fopen()` mengembalikan pointer ke file yang telah dibuka. Dalam program ini, file dibuka dalam mode `r` (read) untuk membaca isi file. Pointer file akan disimpan dalam variabel file.
- Memeriksa apakah file CSV berhasil dibuka atau tidak. Jika file adalah `NULL`, itu berarti file gagal dibuka. Dalam kasus ini, pesan kesalahan akan dicetak ke `stderr` menggunakan `fprintf()`, dan fungsi akan mengembalikan nilai 1, menunjukkan kegagalan dalam membuka file CSV.
- Membuat array karakter `line` dengan ukuran maksimum `MAX_LINE_LENGTH` untuk menyimpan baris yang dibaca dari file CSV.
- Menggunakan fungsi `fgets()` untuk membaca baris pertama (header) dari file CSV yang sudah dibuka. Fungsi `fgets()` membaca baris dari file dan menyimpannya dalam array `line` dengan batas maksimum `MAX_LINE_LENGTH` Dalam program ini, baris pertama (header) dibaca dan disimpan dalam array `line`.

```
// Read player data and print those that meet the criteria
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        // Extract player data from the CSV line
        char* saveptr;
        char* token = strtok_r(line, CSV_DELIMITER, &saveptr);
        char* ID = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Name = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Age = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLPhoto = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Nationality = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLFlag = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Overall = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Potential = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Club = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* URLClublogo = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Value = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Wage = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int Special = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* PreferredFoot = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int InternationalReputation = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int WeakFoot = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int SkillMoves = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* WorkRate = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* BodyType = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* RealFace = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Position = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Joined = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* LoanedFrom = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* ContractValidUntil = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Height = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* Weight = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        char* ReleaseClause = token;
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int KitNumber = atoi(token);
        token = strtok_r(NULL, CSV_DELIMITER, &saveptr);
        int BestOverall = atoi(token);
```
- Kode di atas secara umum berfungsi untuk membaca data pemain dari file CSV yang telah dibuka sebelumnya dan mencetak data pemain yang memenuhi kriteria tertentu. 
- Menggunakan loop `while` untuk membaca baris-baris data dari file CSV yang telah dibuka sebelumnya. Fungsi `fgets()` digunakan untuk membaca baris dari file dan menyimpannya dalam array `line` dengan batas maksimum `MAX_LINE_LENGTH`. Dalam loop ini, setiap baris data akan diolah secara berulang sampai tidak ada baris lagi yang tersedia.
- Pada setiap iterasi loop, baris data yang dibaca akan diuraikan dan masing-masing nilai akan disimpan dalam variabel sesuai dengan jenis datanya. Misalnya, `char* ID = token;` akan menyimpan token yang berisi `ID pemain` ke dalam variabel `ID`, dan seterusnya.
- Pemecahan baris data dilakukan menggunakan fungsi `strtok_r()` yang membagi baris menjadi token-token terpisah berdasarkan delimiter atau pemisah yang diberikan `(CSV_DELIMITER dalam program ini)`.
- Token pertama akan dianggap sebagai `ID pemain`, token kedua sebagai `nama pemain`, token ketiga sebagai `usia` (diubah menjadi tipe data int dengan atoi()), dan seterusnya sesuai dengan urutan data pemain dalam file CSV.

```
 // Remove newline character from tokens
        size_t tokenLength = strcspn(ID, "\n");
        ID[tokenLength] = '\0';

        tokenLength = strcspn(Name, "\n");
        Name[tokenLength] = '\0';

        tokenLength = strcspn(URLPhoto, "\n");
        URLPhoto[tokenLength] = '\0';

        tokenLength = strcspn(Nationality, "\n");
        Nationality[tokenLength] = '\0';

        tokenLength = strcspn(Club, "\n");
        Club[tokenLength] = '\0';
```
- Kode di atas secara umum berfungsi untuk menghapus karakter `newline` (\n) dari token-token yang telah diekstrak sebelumnya. Hal ini dilakukan untuk membersihkan token dari karakter newline yang mungkin ada diakhirnya.
- size_t tokenLength = strcspn(ID, \n`);
Menggunakan fungsi `strcspn()` untuk mencari panjang token ID sebelum karakter newline (\n) ditemukan. Fungsi ini mengembalikan jumlah karakter sebelum karakter newline pertama kali muncul. Panjang token ID akan disimpan dalam variabel `tokenLength`.
- ID[tokenLength] = '\0';
Mengganti karakter newline (\n) pada token `ID` dengan karakter `null` (\0). Ini dilakukan untuk mengakhiri string pada posisi yang benar dan menghapus karakter newline dari token. Setelah operasi ini, token `ID` tidak lagi mengandung karakter newline.
- Langkah-langkah yang sama dilakukan untuk token `Name`, `URLPhoto`, `Nationality`, dan `Club`. Token-token ini masing-masing dimodifikasi dengan cara yang sama untuk menghapus karakter newline. Token `Name`, `URLPhoto`, `Nationality`, dan `Club` akan disesuaikan sehingga tidak lagi mengandung karakter newline setelah langkah-langkah ini.

```
 // Remove double quotes from URLPhoto and Club
        char* quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);
        if (quotePos != NULL) {
            memmove(quotePos, quotePos + 1, strlen(quotePos));
            quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);
            if (quotePos != NULL)
                *quotePos = '\0';
        }

        quotePos = strchr(Club, CSV_QUOTE_CHARACTER[0]);
        if (quotePos != NULL) {
            memmove(quotePos, quotePos + 1, strlen(quotePos));
            quotePos = strchr(Club, CSV_QUOTE_CHARACTER[0]);
            if (quotePos != NULL)
                *quotePos = '\0';
        }
```
- Kode di atas secara umum berfungsi untuk menghapus tanda kutip ganda (double quotes) dari token `URLPhoto` dan `Club`. Hal ini dilakukan untuk membersihkan token dari tanda kutip ganda yang mungkin ada di sekitarnya.
- `char* quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);`
Menggunakan fungsi `strchr()` untuk mencari posisi pertama dari tanda kutip ganda ("). Fungsi ini mengembalikan pointer ke posisi karakter pertama yang ditemukan. Posisi tanda kutip ganda pada token `URLPhoto` akan disimpan dalam variabel `quotePos`.
- `if (quotePos != NULL) { ... }`
Memeriksa apakah tanda kutip ganda ("), yang ditunjukkan oleh `quotePos`, ditemukan dalam token `URLPhoto`. Jika ditemukan, langkah-langkah penghapusan akan dilakukan.
- `memmove(quotePos, quotePos + 1, strlen(quotePos));`
Menggunakan fungsi `memmove()` untuk memindahkan karakter-karakter setelah tanda kutip ganda ke posisi sebelumnya, sehingga menghapus tanda kutip ganda itu sendiri. Ini dilakukan dengan menggeser karakter-karakter ke kiri.
- `quotePos = strchr(URLPhoto, CSV_QUOTE_CHARACTER[0]);`
Mencari posisi tanda kutip ganda yang baru setelah penghapusan pertama dilakukan.
- if (quotePos != NULL) *quotePos = '\0';
Memeriksa apakah tanda kutip ganda masih ada setelah penghapusan pertama. Jika masih ada, karakter di posisi tersebut akan diubah menjadi karakter null (\0), sehingga mengakhiri string pada posisi itu. Setelah langkah-langkah ini, token `URLPhoto` akan tidak lagi mengandung tanda kutip ganda dan siap digunakan dalam pengolahan data selanjutnya.
- Langkah-langkah yang sama dilakukan untuk token `Club` untuk menghapus tanda kutip ganda yang mungkin ada di sekitarnya.

```
  // Check the criteria
        if (Age < 25 && Potential > 85 && strcmp(Club, "Manchester City") != 0) {
            // Print player data that meet the criteria
            printf("Name: %s\n", Name);
            printf("Age: %d\n", Age);
            printf("Club: %s\n", Club);
            printf("Nationality: %s\n", Nationality);
            printf("Potential: %d\n", Potential);
            printf("URL Photo: %s\n", URLPhoto);

            printf("\n");
        }
```
- Kode di atas secara umum berfungsi untuk memeriksa kriteria tertentu pada data pemain yang telah diekstrak sebelumnya dan mencetak data pemain yang memenuhi kriteria tersebut.
- `if (Age < 25 && Potential > 85 && strcmp(Club, "Manchester City") != 0) { ... }`
Memeriksa apakah pemain memenuhi kriteria yang ditentukan. Kriteria yang diperiksa dalam contoh ini adalah Usia `(Age)` pemain harus kurang dari 25, `Potensi` (Potential) pemain harus lebih dari 85, Pemain tidak boleh berasal dari klub `Manchester City` (Club tidak sama dengan `Manchester City`). Jika pemain memenuhi semua kriteria tersebut, langkah-langkah di dalam blok `if` akan dieksekusi.
- Di dalam blok `if`, data pemain yang memenuhi kriteria akan dicetak menggunakan fungsi `printf()`. Dalam program ini mencetak beberapa informasi pemain seperti `nama (Name)`, `usia (Age)`, `klub (Club)`, `kewarganegaraan (Nationality)`, `potensi (Potential)`, dan `URL foto (URLPhoto)`.


**C. Membuat Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.**

Isi dari Dockerfile :

```
FROM ubuntu:latest

WORKDIR /app

COPY . /app

RUN apt-get update && apt-get -y install gcc && apt-get -y install unzip && apt install -y python3-pip

RUN gcc -o storage storage.c

RUN pip install kaggle

RUN mkdir -p /root/.kaggle

ADD kaggle.json /root/.kaggle

COPY kaggle.json /root/.kaggle/

RUN chmod 600 /root/.kaggle/kaggle.json

CMD ["./storage"]
```

Penjelasan isi Dockerfile :

- `FROM ubuntu:latest`: Mendefinisikan base image yang akan digunakan untuk membangun image Docker. Pada program ini, menggunakan image Ubuntu dengan versi terbaru.
- `WORKDIR /app`: Menentukan direktori kerja di dalam container, dalam hal ini adalah /app. Perintah ini membuat direktori /app di dalam container dan mengaturnya sebagai direktori kerja aktif.
- `COPY . /app`: Mengkopi seluruh konten dari direktori saat ini (yang berada di tempat Dockerfile berada) ke direktori /app di dalam container. File-file dan folder yang ada di direktori saat ini akan disalin ke dalam direktori /app di dalam container.
- `RUN apt-get update && apt-get -y install gcc && apt-get -y install unzip && apt install -y python3-pip`: Melakukan beberapa perintah RUN untuk mengupdate package list dalam image Ubuntu (apt-get update) dan menginstal beberapa package seperti GCC, unzip, dan python3-pip.
- `RUN gcc -o storage storage.c`: Mengkompilasi file storage.c menggunakan GCC dan menghasilkan file biner storage. Ini diasumsikan bahwa file storage.c telah ada dalam direktori saat ini dan telah disalin ke dalam direktori /app di dalam container.
- `RUN pip install kaggle`: Menginstal paket kaggle menggunakan pip. Ini akan mengunduh dan menginstal paket kaggle dan semua dependensinya.
- `RUN mkdir -p /root/.kaggle`: Membuat direktori /root/.kaggle di dalam container. Direktori ini akan digunakan untuk menyimpan file kaggle.json.
- `ADD kaggle.json /root/.kaggle`: Menambahkan file kaggle.json dari direktori saat ini ke dalam direktori /root/.kaggle di dalam container. File ini digunakan untuk otentikasi dengan API Kaggle.
- `COPY kaggle.json /root/.kaggle/`: Mengkopi file kaggle.json dari direktori saat ini ke dalam direktori /root/.kaggle/ di dalam container. Ini adalah langkah yang berulang dan mungkin dilakukan untuk memastikan file kaggle.json tersedia dalam direktori yang benar di dalam container.
- `RUN chmod 600 /root/.kaggle/kaggle.json`: Mengubah izin (permission) file kaggle.json menjadi 600 di dalam container. Ini mengatur izin file agar hanya dapat diakses oleh pemiliknya.
- `CMD ["./storage"]`: Menentukan perintah default yang akan dijalankan ketika container berjalan. Dalam hal ini, akan menjalankan file biner storage yang telah dikompilasi sebelumnya.


**D. Mempublish Docker Image sistem ke Docker Hub**

Untuk melakukan push Docker Image kedalam Docker hub dilakukan login terlebih dahulu pada terminal. Kemudian melakukan tag docker image yang diinginkan dengan nama yang sesuai. Terakhir melakukan push dengan perintah :

```
sudo docker push <nama_image>:<tag>
```

**E. Membuat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.**

Isi file docker-compose.yml pada folder Napoli :

```
version: '3'
services:
  myservice1:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8006:80"
  myservice2:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8007:80"
  myservice3:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8008:80"
  myservice4:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8009:80"
  myservice5:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8010:80"
```

Isi file docker-compose.yml pada folder Barcelona :

```
version: '3'
services:
  myservice1:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8001:80"
  myservice2:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8002:80"
  myservice3:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8003:80"
  myservice4:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8004:80"
  myservice5:
    image: iinsanwafi/storage-app:latest
    ports:
      - "8005:80"
```

Docker Compose file di atas digunakan untuk mendefinisikan layanan (services) yang akan dijalankan dalam lingkungan Docker. Dengan Docker Compose ini, Anda dapat menjalankan beberapa instance dari layanan `storage-app` yang berjalan di dalam container Docker, dengan setiap layanan diarahkan ke port yang berbeda di host. Ini memungkinkan Anda untuk menjalankan beberapa instansi aplikasi dengan konfigurasi yang sama, tetapi dapat diakses melalui port yang berbeda.


**Hasil Storage.c ketika di run :**

![](Image/storage.png)


**Bukti telah di push ke DockerHub :**

![](Image/hub.png)


**Kendala yang dialami :**

- Ketika praktikum banyak terjadi eror ketika melakukan instalasi kaggle & docker.




# Soal2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.

Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata `sihir` yaitu `bypass` yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt

Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder.

**Penyelesaiannya:**

pertama mendefinisikan versi `FUSE` yang digunakan, `library` yang akan digunakan dan mendefinisikan konstanta `HOME` dengan nilai `"/home/dilla/sisop_modul_4/jawb2/"`
```
#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <pwd.h>
#include <fuse.h>
#include <errno.h>
#include <fcntl.h>


#define HOME "/home/dilla/sisop_modul_4/jawb2/"
```
lalu selanjutnya buat beberapa function yang akan digunakan untuk membuat `fuse`nya
ada fungsi `getCurrentDateTime` yang mana fungsi ini digunakan untuk mendapatkan tanggal dan waktu saat ini dengan menggunakan fungsi `time`, `localtime`, dan `strftime`. Hasil tanggal dan waktu akan disimpan dalam parameter `dateTime`.
```
int status;
//mendapatkan tanggal
void getCurrentDateTime(char *dateTime) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    setenv("TZ", "Asia/Jakarta", 1); // Set timezone to Jakarta
    timeinfo = localtime(&rawtime);

    strftime(dateTime, 20, "%d/%m/%Y-%H:%M:%S", timeinfo);
}
```
fungsi `get_user` yang mana fungsi ini digunakan untuk mendapatkan nama pengguna `(username)` saat ini dengan menggunakan fungsi `geteuid` dan `getpwuid`. Nama pengguna akan disimpan dalam parameter `username`.
```
//mendapatkan user
void get_user(char *username) {
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);

    if (pw != NULL) {
        strcpy(username, pw->pw_name);
    } else {
        fprintf(stderr, "Failed to get the username.\n");
        exit(1);
    }
}
```
fungsi `write_status_to_file`yang mana fungsi ini digunakan untuk menulis status ke dalam file `log`. Fungsi ini menerima tiga parameter, yaitu `status` (status yang ingin ditulis), `command` (perintah yang sedang dieksekusi), dan `description` (deskripsi tambahan). Fungsi ini membuka file log dengan mode `"a"` (append) dan menulis status, tanggal, perintah, username, dan deskripsi ke dalam file. Setelah penulisan selesai, file log ditutup.
```
//Menuliskan status ke txt
void write_status_to_file(const char *status, const char *command, const char *description) {
    char datetime[20];
    char username[256];
    getCurrentDateTime(datetime);
    get_user(username);

    FILE *logFile = fopen("/home/dilla/sisop_modul_4/jawb2/logmucatatsini.txt", "a");
    if (logFile != NULL) {
        fprintf(logFile, "[%s]::%s::%s::%s-%s\n", status, datetime, command, username, description);
        // fprintf(logFile, "test");
        fclose(logFile);
    } else {
        printf("Failed to open log file.\n");
    }
}
```
fungsi `download_file` yang mana fungsi ini digunakan untuk mendownload file dari `URL` yang diberikan menggunakan perintah `wget`. Fungsi ini menerima dua parameter, yaitu `linkUrl` (URL file yang ingin didownload) dan `saveDir` (lokasi penyimpanan file yang diunduh). Fungsi ini melakukan `fork` untuk menjalankan perintah `wget` dalam proses `child`. Setelah proses download selesai, fungsi menunggu child process selesai menggunakan fungsi `wait`. Jika proses download berhasil (exit status 0), fungsi `write_status_to_file` dipanggil dengan status `"SUCCESS"`. Jika proses download gagal (exit status bukan 0), fungsi `write_status_to_file` dipanggil dengan status `"FAILED"`.
```
//Dowload file dari drive
void download_file(char *linkUrl, char *saveDir){
  char *link = malloc(100); 


    strcpy(link, linkUrl); // meng-copy isi dari variabel `linkUrl` ke variabel `link`
    pid_t pid = fork(); //Deklarasi ID, proses keberapa
    if (pid == 0){ 
       char *argv[] = {"wget","--quiet", "--no-check-certificate", link, "-O", saveDir, NULL};
       execv("/usr/bin/wget", argv);
        exit(0);
    }
    wait(&status);// menunggu child process selesai
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        write_status_to_file("SUCCESS", "DOWNLOAD", "Download completed successfully.");
    } else {
        write_status_to_file("FAILED", "DOWNLOAD", "Download failed.");
    }
}
```
fungsi `unzipFile` yang mana fungsi ini digunakan untuk mengekstrak file ZIP menggunakan perintah `unzip`. Fungsi ini menerima dua parameter, yaitu `zipFile` (lokasi file ZIP yang ingin diekstrak) dan `unzipDir` (lokasi direktori tujuan hasil ekstraksi). Fungsi ini melakukan `fork` untuk menjalankan perintah unzip dalam proses child. Setelah proses ekstraksi selesai, fungsi menunggu child process selesai menggunakan fungsi `wait`. Jika proses ekstraksi berhasil (exit status 0), fungsi `write_status_to_file` dipanggil dengan status `"SUCCESS"`. Jika proses ekstraksi gagal (exit status bukan 0), fungsi `write_status_to_file` dipanggil dengan status `"FAILED"`.
```
//percobaan unzip 
void unzipFile(char *zipFile, char *unzipDir) {
    pid_t pid;
    int status;

    if (access(zipFile, F_OK) == -1) {
        printf("Zip file not found.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) { 
        freopen("/dev/null", "w", stdout); //mengarahkan output ke /dev/null
        execlp("unzip", "unzip", "-o", zipFile, "-d",  unzipDir, NULL);
        exit(1); 
    } else if (pid > 0) { // parent process
        while (wait(&status) != pid); //menunggu child process selesai
    } else { 
        perror("fork"); //jika fork gagal maka menampilkan error
        exit(1);
    }

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        write_status_to_file("SUCCESS", "UNZIP", "Unzip completed successfully.");
    } else {
        write_status_to_file("FAILED", "UNZIP", "Unzip failed.");
    }
}

static  const  char *dirpath = "/home/dilla/sisop_modul_4/jawb2/nanaxgerma/src_data";
```
Dan yang terakhir implementasi fungsi-fungsi yang akan dilakukan dalam fuse, yaitu
fungsi `xmp_getattr` yang mana fungsi ini merupakan implementasi dari operasi `getattr` dalam `FUSE`. Digunakan untuk mendapatkan atribut file seperti ukuran, waktu modifikasi, dan lain-lain.
```
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) 
    
    return -errno;

    return 0;
}
```
fungsi `xmp_readdir` yang mana fungsi ini merupakan implementasi dari operasi `readdir` dalam `FUSE`. Digunakan untuk membaca isi direktori.
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}
```
fungsi `xmp_read` yang mana fungsi ini merupakan implementasi dari operasi `read` dalam `FUSE`. Digunakan untuk `membaca isi file`.
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```
fungsi `xmp_mkdir` yang mana fungsi ini merupakan implementasi dari operasi `mkdir` dalam `FUSE`. Digunakan untuk `membuat direktori`.
```
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Create directory %s", path);
    
    // Pengecekan bypass
    if (strstr(path, "bypass") != NULL)
    {
        // Jika path mengandung "bypass", lanjutkan pembuatan direktori
        int res = mkdir(fpath, mode);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return res
            write_status_to_file("SUCCESS", "MKDIR", text);
            return res;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "MKDIR", text);
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass" dan "restricted", lanjutkan pembuatan direktori
        int res = mkdir(fpath, mode);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return res
            write_status_to_file("SUCCESS", "MKDIR", text);
            return res;
        }
    }
}
```
fungsi `xmp_create` yang mana fungsi ini merupakan implementasi dari operasi `create` dalam `FUSE`. Digunakan untuk `membuat file baru`.
```
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Create file %s", path);

    // Pengecekan bypass dan restricted
    if (strstr(path, "bypass") != NULL && strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "bypass" dan "restricted", lanjutkan pembuatan file
        int fd = creat(fpath, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan close file descriptor
            write_status_to_file("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "CREATE", "Failed to create file");
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass" dan "restricted", lanjutkan pembuatan file
        int fd = creat(fpath, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan close file descriptor
            write_status_to_file("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
}
```
fungsi `xmp_rename` yang mana fungsi ini merupakan implementasi dari operasi `rename` dalam `FUSE`. Digunakan untuk `mengubah nama file` atau `memindahkan file`.
```
static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    char text[1000];
    
    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(fpath_from, "%s", from);
    }
    else
    {
        sprintf(fpath_from, "%s%s", dirpath, from);
    }

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fpath_to, "%s", to);
    }
    else
    {
        sprintf(fpath_to, "%s%s", dirpath, to);
    }

    sprintf(text, "Rename from %s to %s" , from, to);

    // Pengecekan bypass pada path "from" atau "to"
    if (strstr(from, "bypass") != NULL || strstr(to, "bypass") != NULL)
    {
        // Jika terdapat kata "bypass" pada path "from" atau "to", dilakukan rename
        int res = rename(fpath_from, fpath_to);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAIL", "RENAME", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RENAME", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak mengandung "bypass", log gagal dan return -EPERM
        write_status_to_file("FAIL", "RENAME", "Renaming file failed due to bypass restriction");
        return -EPERM;
    }
}
```
fungsi `xmp_unlink` yang mana fungsi ini merupakan implementasi dari operasi `unlink` dalam `FUSE`. Digunakan untuk `menghapus file`.
```
static int xmp_unlink(const char *path)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Remove file %s", path);

    // Pengecekan kata "restricted" dan "bypass" pada path
    if (strstr(path, "restricted") != NULL && strstr(path, "bypass") != NULL)
    {
        int res = unlink(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung hanya "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
        return -1;
    }
    else
    {
        // Jika tidak mengandung "restricted" dan "bypass", lanjutkan penghapusan file
        int res = unlink(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
}
```
fungsi `xmp_rmdir` yang mana  fungsi ini merupakan implementasi dari operasi `rmdir` dalam `FUSE`. Digunakan untuk `menghapus direktori`.
```
static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Remove directory %s", path);

    // Pengecekan bypass
    if (strstr(path, "bypass") != NULL)
    {
        // Jika path mengandung "bypass", lanjutkan penghapusan direktori
        int res = rmdir(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAIL", "REMOVE1", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "REMOVE", text);
            return 0;
        }
    }
    // Pengecekan restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAIL", "REMOVE2", text);
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass", log gagal dan return -1
        write_status_to_file("FAIL", "REMOVE3", text);
        return -1;
    }
}
```
fungsi `fuse_operations xmp_oper` untuk menginisialisasi semua dungsi-fungsi diatas seperti mkdir, rmdir, dll.
```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr, //mendapatkan atribut file
    .readdir = xmp_readdir, 
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .create = xmp_create, 
    .rename = xmp_rename, 
    .unlink = xmp_unlink, 
    .rmdir = xmp_rmdir,  
};
```
fungsi `init()` yang berisi beberapa operasi yang dijalankan saat program diinisialisasi. Pada fungsi `init()`, dilakukan pengunduhan file dari URL menggunakan `download_file()`, kemudian file tersebut di-unzip menggunakan `unzipFile()`. Dalam contoh ini, file yang diunduh adalah `"nanaxgerma.zip"`, dan file tersebut di-unzip ke direktori `"/home/dilla/sisop_modul_4/jawb2"`.
```
void init(){
    char *url = "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i";
    download_file(url, "./nanaxgerma.zip");
    // change_permission(HOME"/nanaxgerma.zip");
    // unzipFile("/home/naraduhita/sisop/", "/home/naraduhita/sisop/nanaxgerma.zip"); //unzip binatang.zip
    char* zipFile = "nanaxgerma.zip";
    char* unzipDir = "/home/dilla/sisop_modul_4/jawb2";
    unzipFile(zipFile, unzipDir);
    }
```
fungsi `main()`, terdapat inisialisasi variabel pid dengan nilai hasil dari pemanggilan `fork()`. Pada proses parent (pid > 0), program langsung keluar dengan menggunakan `exit()`. Pada proses child (pid = 0), dilakukan pemanggilan fungsi init() untuk melakukan inisialisasi, kemudian dilakukan pengaturan permission dengan `umask(0)`. Terakhir, program memanggil `fuse_main()` untuk menjalankan `FUSE` dengan menggunakan fungsi-fungsi yang didefinisikan sebelumnya, yaitu `xmp_oper`.
```
int main(int argc, char *argv[]) {
    pid_t pid; //Deklarasi sebuah variabel bertipe pid_t 

    pid = fork(); 

    if (pid<0){
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    init();
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);

    return 0;
}
```

**Output:**

a. Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.

b. Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.

c. Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

d. Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

![] (Image/mkdir_1.png)
# Soal5

Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.	

Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.

Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.

Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.

Contoh:

kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce

Penjelasan:
kyunkyun adalah username.
fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 

Catatan: 
Tidak perlu ada password validation untuk mempermudah kalian.		
Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.

Contoh:

A01_a.pdf
xP4UcxRZE5_A01


List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.
Contoh: 
result.txt


extension.txt
folder = 12
jpg = 13
png = 2
.
.
.
dan seterusnya


**Penyelesaiannya:**

Untuk menyelesaikan soal no 5, diperlukan integrasi FUSE dengan Docker,FUSE disini akan di pakai untuk mount tipe volumes yang mana mengizinkan Docker container untuk mengakses filessytem host OS 

Pertama-tama kita tulis header dan menginisialisasikan beberapa variabel global  dalam rahasia.c terlebih dahulu 

```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/wait.h>

static  const  char *dirpath = "/home/ahyun/Documents/prakmod4/rahasia";

```
Selanjutnya masuk di fungsi xmp_getattr yang digunakan untuk mendapatkan atribut (metadata) dari sebuah file yang diidentifikasi oleh path
```
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);
    if (res == -1) return -errno;
    return 0;
}
```
int xmp_getattr(const char *path, struct stat *stbuf): Ini adalah deklarasi fungsi xmp_getattr yang mengambil dua parameter. path adalah string yang berisi jalur file yang ingin diambil atributnya, sedangkan stbuf adalah pointer ke struktur stat yang akan berisi atribut file setelah pemanggilan fungsi ini.

char fpath[1000];: Mendeklarasikan array karakter fpath dengan kapasitas 1000. Array ini akan digunakan untuk menyimpan jalur lengkap file yang akan diakses.
sprintf(fpath,"%s%s",dirpath,path);: Menggabungkan jalur direktori dirpath dengan path untuk membentuk jalur lengkap file dan menyimpannya di fpath. %s merupakan placeholder yang akan digantikan oleh string dirpath dan path.
res = lstat(fpath, stbuf);: Memanggil fungsi lstat dengan argumen fpath dan stbuf. Fungsi lstat digunakan untuk mengambil atribut file dari jalur yang ditentukan dan menyimpannya dalam struktur stat yang ditunjuk oleh stbuf. Nilai pengembalian lstat akan disimpan dalam variabel res.
if (res == -1) return -errno;: Memeriksa nilai kembalian res dari pemanggilan lstat. Jika nilainya adalah -1, artinya terjadi kesalahan dalam pemanggilan lstat. Dalam hal ini, fungsi akan mengembalikan nilai -errno, yang merupakan variabel yang menyimpan kode kesalahan terkait dengan kesalahan yang terjadi.
return 0;: Jika tidak ada kesalahan, fungsi akan mengembalikan nilai 0 sebagai indikasi bahwa atribut file telah berhasil diambil dan disimpan dalam stbuf.

lalu kita masuk ke fungsi xmp_readdir Fungsi ini digunakan untuk membaca isi dari sebuah direktori yang diidentifikasi oleh path dan mengisi buffer buf dengan informasi file yang ditemukan


```

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    
    else sprintf(fpath, "%s%s", dirpath, path);
    int res = 0;

    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        printf("Debug: Failed to open directory: %s\n", fpath); 
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));


        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char entry_path[5000];
        sprintf(entry_path, "%s/%s", fpath, de->d_name);

        if (stat(entry_path, &st) == -1) {
            return -errno;
        }
        
        char new_entry_path[5000];
        if (S_ISDIR(st.st_mode)) {

            if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {

                char new_name[2000];
                sprintf(new_name, "%s_E08", de->d_name);

                // Mengecek apakah kata 'E08' sudah ada dalam nama folder sebelum melakukan rename
                if (strstr(de->d_name, "E08") == NULL) {
                    // Melakukan rename folder
                    sprintf(entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);
                    if (rename(entry_path, new_entry_path) == -1) {
                        printf("Debug: Failed to rename folder: %s\n", entry_path);
                    }

                    res = xmp_readdir(entry_path, buf, filler, offset, fi);
                }
            }

        }
        //cek apakah termasuk file
        if(S_ISREG(st.st_mode)) {
            
            //apakah nama file mengandung "E08"
            if (strstr(de->d_name, "E08") == NULL) {
                char new_name[2000];
                char *ext = strrchr(de->d_name, '.');
                if (ext != NULL) {
                    sprintf(new_name, "E08_%s", de->d_name);

                    char new_entry_path[5000];
                    char old_entry_path[5000];
                    sprintf(old_entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);

                    // Memeriksa keberadaan file sebelum melakukan rename
                    if (access(new_entry_path, F_OK) == -1) {
                        // File dengan nama yang diinginkan belum ada, lanjutkan dengan proses rename
                        if (rename(entry_path, new_entry_path) == -1) {
                            printf("Debug: Failed to rename file: %s\n", entry_path);
                            perror("Error"); // Menampilkan pesan error lebih spesifik
                        }
                    } 
                }    
            }
        }

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0) break;
    }
    closedir(dp);
    return 0;
}


```

int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi): Ini adalah deklarasi fungsi xmp_readdir yang mengambil lima parameter. path adalah string yang berisi jalur direktori yang ingin dibaca, buf adalah pointer ke buffer yang akan diisi dengan informasi file, filler adalah fungsi pengisian buffer, offset adalah posisi dalam direktori yang akan digunakan sebagai titik awal pembacaan, dan fi adalah pointer ke struktur fuse_file_info yang menyimpan informasi file terkait.
char fpath[1000];: Mendeklarasikan array karakter fpath dengan kapasitas 1000. Array ini akan digunakan untuk menyimpan jalur lengkap direktori yang akan diakses.
if (strcmp(path, "/") == 0) {...}: Memeriksa apakah path merupakan akar direktori ("/"). Jika benar, path akan diubah menjadi dirpath, dan fpath akan diisi dengan nilai dirpath sehingga kita dapat membaca isi direktori root.
else sprintf(fpath, "%s%s", dirpath, path);: Jika path bukan merupakan akar direktori, maka fpath akan diisi dengan gabungan dirpath dan path sehingga kita dapat membaca isi direktori yang spesifik.
int res = 0;: Mendeklarasikan variabel res dengan nilai awal 0. Variabel ini akan digunakan untuk menyimpan nilai pengembalian dari fungsi filler yang memasukkan informasi file ke dalam buffer.
DIR *dp;: Mendeklarasikan pointer dp yang akan digunakan untuk menyimpan alamat direktori yang akan dibuka.
struct dirent *de;: Mendeklarasikan pointer de yang akan digunakan untuk menyimpan entri direktori saat melakukan pembacaan.
(void)offset; (void)fi;: Mengabaikan parameter offset dan fi yang tidak digunakan dalam fungsi ini. Hal ini dilakukan untuk menghindari adanya warning saat kompilasi.
dp = opendir(fpath);: Membuka direktori dengan jalur yang sudah disiapkan dan menyimpan alamat direktori dalam dp.
if (dp == NULL) {...}: Memeriksa apakah pembukaan direktori berhasil. Jika tidak berhasil (dp bernilai NULL), maka akan menampilkan pesan kesalahan dan mengembalikan nilai -errno yang menunjukkan kode kesalahan yang terjadi.
while ((de = readdir(dp)) != NULL) {...}: Melakukan loop untuk membaca entri-entri direktori satu per satu menggunakan fungsi readdir. Loop akan berjalan selama ada entri yang dibaca.
struct stat st; memset(&st, 0, sizeof(st));: Mendeklarasikan variabel `st

selanjutnya kita masuk ke dalam fungsi xmp_read Fungsi ini digunakan untuk membaca isi dari sebuah file yang diidentifikasi oleh path dan menyimpannya ke dalam buffer buf

```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0){
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;
    close(fd);

    return res;
}

```
int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi): Ini adalah deklarasi fungsi xmp_read yang mengambil lima parameter. path adalah string yang berisi jalur file yang ingin dibaca, buf adalah pointer ke buffer yang akan diisi dengan isi file, size adalah ukuran maksimum yang ingin dibaca, offset adalah posisi dalam file yang akan digunakan sebagai titik awal pembacaan, dan fi adalah pointer ke struktur fuse_file_info yang menyimpan informasi file terkait.
char fpath[1000];: Mendeklarasikan array karakter fpath dengan kapasitas 1000. Array ini akan digunakan untuk menyimpan jalur lengkap file yang akan dibaca.
if(strcmp(path,"/") == 0){...}: Memeriksa apakah path merupakan akar direktori ("/"). Jika benar, path akan diubah menjadi dirpath, dan fpath akan diisi dengan nilai dirpath sehingga kita dapat membaca file di root direktori.
else sprintf(fpath, "%s%s",dirpath,path);: Jika path bukan merupakan akar direktori, maka fpath akan diisi dengan gabungan dirpath dan path sehingga kita dapat membaca file yang spesifik.
int res = 0;: Mendeklarasikan variabel res dengan nilai awal 0. Variabel ini akan digunakan untuk menyimpan nilai pengembalian dari operasi pembacaan.
int fd = 0;: Mendeklarasikan variabel fd dengan nilai awal 0. Variabel ini akan digunakan untuk menyimpan file descriptor setelah pembukaan file.
(void) fi;: Mengabaikan parameter fi yang tidak digunakan dalam fungsi ini. Hal ini dilakukan untuk menghindari adanya warning saat kompilasi.
fd = open(fpath, O_RDONLY);: Membuka file dengan jalur yang sudah disiapkan dalam mode hanya pembacaan (O_RDONLY) dan menyimpan file descriptor dalam fd.
if (fd == -1) return -errno;: Memeriksa apakah pembukaan file berhasil. Jika tidak berhasil (fd bernilai -1), maka fungsi akan mengembalikan nilai -errno yang menunjukkan kode kesalahan yang terjadi.
res = pread(fd, buf, size, offset);: Membaca isi file dengan menggunakan fungsi pread. Fungsi ini membaca size byte dari file dengan file descriptor fd, dimulai dari offset, dan menyimpannya ke dalam buffer buf. Nilai pengembalian dari pread akan disimpan dalam res.
if (res == -1) res = -errno;: Memeriksa apakah operasi pembacaan file berhasil. Jika tidak berhasil (res bernilai -1), maka res akan diubah menjadi -errno yang menunjukkan

Selanjutnya kita masuk ke dalam fungsi xmp_rename Fungsi ini digunakan untuk mengubah nama (rename) sebuah file atau direktori dari from menjadi to

```

static int xmp_rename(const char *from, const char *to)
{
    char fpathFrom[1000];
    char fpathTo[1000];

    sprintf(fpathFrom, "%s%s", dirpath, from);
    sprintf(fpathTo, "%s%s", dirpath, to);

    printf("rename: %s to %s\n", fpathFrom, fpathTo);

    int res = rename(fpathFrom, fpathTo);
    if (res == -1)
        return -errno;

    return 0;
}



```
int xmp_rename(const char *from, const char *to): Ini adalah deklarasi fungsi xmp_rename yang mengambil dua parameter. from adalah string yang berisi jalur asal (nama sebelumnya) dari file atau direktori yang akan direname, sedangkan to adalah string yang berisi jalur tujuan (nama baru) yang diinginkan untuk file atau direktori tersebut.
char fpathFrom[1000]; dan char fpathTo[1000];: Mendeklarasikan dua array karakter fpathFrom dan fpathTo dengan kapasitas 1000. Array ini akan digunakan untuk menyimpan jalur lengkap dari file atau direktori yang akan direname.
sprintf(fpathFrom, "%s%s", dirpath, from); dan sprintf(fpathTo, "%s%s", dirpath, to);: Menggabungkan dirpath (jalur direktori utama) dengan from dan to menggunakan fungsi sprintf. Hasilnya akan disimpan dalam fpathFrom dan fpathTo, sehingga kita dapat memperoleh jalur lengkap dari file atau direktori yang akan direname.
printf("rename: %s to %s\n", fpathFrom, fpathTo);: Menampilkan pesan ke konsol untuk memperlihatkan jalur asal dan tujuan dari proses rename yang akan dilakukan.
int res = rename(fpathFrom, fpathTo);: Melakukan operasi rename menggunakan fungsi rename. Fungsi ini akan mengubah nama file atau direktori dari fpathFrom menjadi fpathTo. Nilai pengembalian dari rename akan disimpan dalam res.
if (res == -1) return -errno;: Memeriksa apakah operasi rename berhasil. Jika tidak berhasil (res bernilai -1), maka fungsi akan mengembalikan nilai -errno yang menunjukkan kode kesalahan yang terjadi.
return 0;: Mengembalikan nilai 0 untuk menunjukkan bahwa proses rename telah berhasil dilakukan tanpa ada kesalahan.

Lalu kita Buat struct untuk operasi fuse 

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

```
lalu kita masuk ke fungsi main 

```

int  main(int  argc, char *argv[])
{
    int cid;
    cid = fork();
    if(cid==0)
        system("wget -O rahasia.zip \"https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes\"");
    else if(cid>0){
        wait(NULL);
        system("unzip rahasia.zip");

        int cid2;
        cid2=fork();
        if(cid2==0){
            umask(0);
            fuse_main(argc, argv, &xmp_oper, NULL);
        }else if(cid2>0){
            wait(NULL);
            system("docker-compose up -d");
            
            /*scriptregis dan scriptext 100% chatgpt*/
            const char* scriptregis =
            "#!/bin/bash\n"
            "\n"
            "check_username_exists() {\n"
            "    local username=$1\n"
            "    grep -q \"^$username;\" credentials.txt\n"
            "}\n"
            "\n"
            "register_user() {\n"
            "    local username=$1\n"
            "    local password=$2\n"
            "\n"
            "    if check_username_exists \"$username\"; then\n"
            "        echo \"Username already exists. Please choose a different username.\"\n"
            "    else\n"
            "        hashed_password=$(echo -n \"$password\" | md5sum | awk '{print $1}')\n"
            "        echo \"$username;$hashed_password\" >> credentials.txt\n"
            "        echo \"User registered successfully.\"\n"
            "    fi\n"
            "}\n"
            "\n"
            "login_user() {\n"
            "    local username=$1\n"
            "    local password=$2\n"
            "\n"
            "    if check_username_exists \"$username\"; then\n"
            "        hashed_password=$(echo -n \"$password\" | md5sum | awk '{print $1}')\n"
            "        stored_password=$(grep \"^$username;\" credentials.txt | cut -d';' -f2)\n"
            "        if [ \"$hashed_password\" = \"$stored_password\" ]; then\n"
            "            echo \"Login successful.\"\n"
            "            docker exec -it prakmod4_no5 /bin/bash\n"
            "        else\n"
            "            echo \"Incorrect password.\"\n"
            "        fi\n"
            "    else\n"
            "        echo \"Username not found.\"\n"
            "    fi\n"
            "}\n"
            "\n"
            "while true; do\n"
            "    echo \"1. Register\"\n"
            "    echo \"2. Login\"\n"
            "    echo \"3. Exit\"\n"
            "    read -p \"Enter your choice: \" choice\n"
            "    case $choice in\n"
            "        1)\n"
            "            read -p \"Enter username: \" username\n"
            "            read -p \"Enter password: \" password\n"
            "            register_user \"$username\" \"$password\"\n"
            "            ;;\n"
            "        2)\n"
            "            read -p \"Enter username: \" username\n"
            "            read -p \"Enter password: \" password\n"
            "            login_user \"$username\" \"$password\"\n"
            "            ;;\n"
            "        3)\n"
            "            break\n"
            "            ;;\n"
            "        *)\n"
            "            echo \"Invalid choice. Please try again.\"\n"
            "            ;;\n"
            "    esac\n"
            "    echo\n"
            "done\n";

            const char *scriptext =
            "#!/bin/bash\n\n"
            "directory=\"/home/ahyun/Documents/prakmod4/rahasia\"\n\n"
            "output_file=\"extension.txt\"\n\n"
            "count_files() {\n"
            "    local search_dir=\"$1\"\n"
            "    local extensions=()\n"
            "    local dir_count=0\n\n"
            "    while IFS= read -r -d '' file; do\n"
            "        if [[ -f \"$file\" ]]; then\n"
            "            ext=\"${file##*.}\"\n"
            "            if [[ -n \"$ext\" && ! \" ${extensions[@]} \" =~ \" $ext \" ]]; then\n"
            "                extensions+=(\"$ext\")\n"
            "            fi\n"
            "        elif [[ -d \"$file\" ]]; then\n"
            "            ((dir_count++))\n"
            "        fi\n"
            "    done < <(find \"$search_dir\" -print0)\n\n"
            "    for ext in \"${extensions[@]}\"; do\n"
            "        count=$(find \"$search_dir\" -type f -iname \"*.$ext\" | wc -l)\n"
            "        echo \"$ext: $count\" >> \"$output_file\"\n"
            "    done\n\n"
            "    echo \"Directories: $dir_count\" >> \"$output_file\"\n"
            "}\n\n"
            "count_files \"$directory\"";

            system(scriptregis);
            system("tree rahasia > result.txt");

            system("touch iniskripext.sh");
            FILE* file = fopen("iniskripext.sh", "w");
            if (file) {
                fprintf(file, "%s", scriptext);
                fclose(file);
            }
            system("bash iniskripext.sh");
        }
    }
    return 0;
}
```

Fungsi ini digunakan untuk mengubah nama (rename) sebuah file atau direktori dari from menjadi to





int main(int argc, char *argv[]): Ini adalah deklarasi fungsi main yang mengambil argumen argc (jumlah argumen baris perintah) dan argv (array argumen baris perintah).
int cid;: Mendeklarasikan variabel cid bertipe int yang akan digunakan untuk menyimpan nilai hasil dari fork.
cid = fork();: Memanggil fungsi fork untuk membuat proses baru. Nilai yang dikembalikan oleh fork akan disimpan dalam cid. Jika cid sama dengan 0, maka ini menandakan bahwa kita berada dalam proses child. Jika cid lebih besar dari 0, maka ini menandakan bahwa kita berada dalam proses parent.
if (cid == 0): Memeriksa apakah kita berada dalam proses child.
system("wget -O rahasia.zip \"https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes\"");: Jika iya, maka menjalankan perintah wget menggunakan fungsi system. Perintah ini digunakan untuk mengunduh file dengan URL yang diberikan dan menyimpannya dengan nama rahasia.zip.
else if (cid > 0): Memeriksa apakah kita berada dalam proses parent.
wait(NULL);: Menunggu proses child selesai menggunakan fungsi wait. Dalam hal ini, kita menunggu proses pengunduhan file rahasia.zip selesai sebelum melanjutkan eksekusi program.
system("unzip rahasia.zip");: Menjalankan perintah unzip menggunakan fungsi system. Perintah ini digunakan untuk mengekstrak isi dari file rahasia.zip.
int cid2;: Mendeklarasikan variabel cid2 bertipe int yang akan digunakan untuk menyimpan nilai hasil dari fork kedua.
cid2 = fork();: Memanggil fungsi fork kedua untuk membuat proses baru. Nilai yang dikembalikan oleh fork akan disimpan dalam cid2.
if (cid2 == 0): Memeriksa apakah kita berada dalam proses child kedua.
umask(0);: Menjalankan perintah umask dengan argumen 0 menggunakan fungsi umask. Perintah ini digunakan untuk mengatur nilai umask ke 0, sehingga hak akses file yang dibuat selanjutnya akan menggunakan hak akses default.
fuse_main(argc, argv, &xmp_oper, NULL);: Memanggil fungsi fuse_main yang merupakan inti dari FUSE (Filesystem in Userspace) untuk menjalankan file sistem virtual. Fungsi ini mengambil argumen argc dan `argv

Setelah membuat rahasia.c kita lanjutkan membuat Docker-compose.yml yang di dalamnya berisi 

```
version: "3"
services:
  rahasiaku:
    build:
      context: .
      dockerfile: Dockerfile
    image: rahasia_di_docker_e08
    container_name: prakmod4_no5
    volumes:
      - /home/ahyun/Documents/prakmod4/rahasia:/usr/share/rahasia
    command: tail -f /dev/null
```
Setelah membuat rahasia.c kita lanjutkan membuat Dockerfile yang di dalamnya berisi 
```
FROM ubuntu:focal
RUN apt-get update && apt-get install -y fuse libfuse-dev libssl-dev libcurl4-openssl-dev gcc make

WORKDIR /usr/share/rahasia

CMD tail -f /dev/null

```
lalu kita compile dengan 
```
gcc -Wall `pkg-config fuse --cflags` rahasia.c -o prakmod04 `pkg-config fuse --libs` -lm

```
Berikut adalah hasil dari soal no 5 

![](Image/no_5.png)
![](Image/no_5__2_.png)
![](Image/no_5__3_.png)
![](Image/no_5__4_.png)


