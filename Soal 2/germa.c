#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <pwd.h>
#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
// #include <dirent.h>

#define HOME "/home/ilham/sisopmodul4/soal2/"

int status;
//mendapatkan tanggal
void getCurrentDateTime(char *dateTime) {
    time_t rawtime;
    struct tm *timeinfo;

    time(&rawtime);
    setenv("TZ", "Asia/Jakarta", 1); // Set timezone to Jakarta
    timeinfo = localtime(&rawtime);

    strftime(dateTime, 20, "%d/%m/%Y-%H:%M:%S", timeinfo);
}
//mendapatkan user
void get_user(char *username) {
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);

    if (pw != NULL) {
        strcpy(username, pw->pw_name);
    } else {
        fprintf(stderr, "Failed to get the username.\n");
        exit(1);
    }
}
//Menuliskan status ke txt
void write_status_to_file(const char *status, const char *command, const char *description) {
    char datetime[20];
    char username[256];
    getCurrentDateTime(datetime);
    get_user(username);

    FILE *logFile = fopen("/home/ilham/sisopmodul4/soal2/logmucatatsini.txt", "a");
    if (logFile != NULL) {
        fprintf(logFile, "[%s]::%s::%s::%s-%s\n", status, datetime, command, username, description);
        // fprintf(logFile, "test");
        fclose(logFile);
    } else {
        printf("Failed to open log file.\n");
    }
}

//Dowload file dari drive
void download_file(char *linkUrl, char *saveDir){
  char *link = malloc(100); 


    strcpy(link, linkUrl); // meng-copy isi dari variabel `linkUrl` ke variabel `link`
    pid_t pid = fork(); //Deklarasi ID, proses keberapa
    if (pid == 0){ 
       char *argv[] = {"wget","--quiet", "--no-check-certificate", link, "-O", saveDir, NULL};
       execv("/usr/bin/wget", argv);
        exit(0);
    }
    wait(&status);// menunggu child process selesai
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        write_status_to_file("SUCCESS", "DOWNLOAD", "Download completed successfully.");
    } else {
        write_status_to_file("FAILED", "DOWNLOAD", "Download failed.");
    }
}

//percobaan unzip 2
void unzipFile(char *zipFile, char *unzipDir) {
    pid_t pid;
    int status;

    if (access(zipFile, F_OK) == -1) {
        printf("Zip file not found.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) { 
        freopen("/dev/null", "w", stdout); //mengarahkan output ke /dev/null
        execlp("unzip", "unzip", "-o", zipFile, "-d",  unzipDir, NULL);
        exit(1); 
    } else if (pid > 0) { // parent process
        while (wait(&status) != pid); //menunggu child process selesai
    } else { 
        perror("fork"); //jika fork gagal maka menampilkan error
        exit(1);
    }

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        write_status_to_file("SUCCESS", "UNZIP", "Unzip completed successfully.");
    } else {
        write_status_to_file("FAILED", "UNZIP", "Unzip failed.");
    }
}

static  const  char *dirpath = "/home/ilham/sisopmodul4/soal2/nanaxgerma/src_data";

//mencari file, backup data, manajemen file
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) 
    
    return -errno;

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Create directory %s", path);
    
    // Pengecekan bypass
    if (strstr(path, "bypass") != NULL)
    {
        // Jika path mengandung "bypass", lanjutkan pembuatan direktori
        int res = mkdir(fpath, mode);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return res
            write_status_to_file("SUCCESS", "MKDIR", text);
            return res;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "MKDIR", text);
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass" dan "restricted", lanjutkan pembuatan direktori
        int res = mkdir(fpath, mode);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "MKDIR", "Creating directory failed");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return res
            write_status_to_file("SUCCESS", "MKDIR", text);
            return res;
        }
    }
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Create file %s", path);

    // Pengecekan bypass dan restricted
    if (strstr(path, "bypass") != NULL && strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "bypass" dan "restricted", lanjutkan pembuatan file
        int fd = creat(fpath, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan close file descriptor
            write_status_to_file("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "CREATE", "Failed to create file");
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass" dan "restricted", lanjutkan pembuatan file
        int fd = creat(fpath, mode);
        if (fd == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "CREATE", "Failed to create file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan close file descriptor
            write_status_to_file("SUCCESS", "CREATE", text);
            close(fd);
            return 0;
        }
    }
}
static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    char text[1000];
    
    if (strcmp(from, "/") == 0)
    {
        from = dirpath;
        sprintf(fpath_from, "%s", from);
    }
    else
    {
        sprintf(fpath_from, "%s%s", dirpath, from);
    }

    if (strcmp(to, "/") == 0)
    {
        to = dirpath;
        sprintf(fpath_to, "%s", to);
    }
    else
    {
        sprintf(fpath_to, "%s%s", dirpath, to);
    }

    sprintf(text, "Rename from %s to %s" , from, to);

    // Pengecekan bypass pada path "from" atau "to"
    if (strstr(from, "bypass") != NULL || strstr(to, "bypass") != NULL)
    {
        // Jika terdapat kata "bypass" pada path "from" atau "to", dilakukan rename
        int res = rename(fpath_from, fpath_to);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAIL", "RENAME", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RENAME", text);
            return 0;
        }
    }
    else
    {
        // Jika tidak mengandung "bypass", log gagal dan return -EPERM
        write_status_to_file("FAIL", "RENAME", "Renaming file failed due to bypass restriction");
        return -EPERM;
    }
}

//Hapus file
static int xmp_unlink(const char *path)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Remove file %s", path);

    // Pengecekan kata "restricted" dan "bypass" pada path
    if (strstr(path, "restricted") != NULL && strstr(path, "bypass") != NULL)
    {
        int res = unlink(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
    else if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung hanya "restricted", log gagal dan return -1
        write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
        return -1;
    }
    else
    {
        // Jika tidak mengandung "restricted" dan "bypass", lanjutkan penghapusan file
        int res = unlink(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAILED", "RMFILE", "Failed to remove file");
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "RMFILE", text);
            return 0;
        }
    }
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    char text[100];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    sprintf(text, "Remove directory %s", path);

    // Pengecekan bypass
    if (strstr(path, "bypass") != NULL)
    {
        // Jika path mengandung "bypass", lanjutkan penghapusan direktori
        int res = rmdir(fpath);
        if (res == -1)
        {
            // Jika terjadi error, log gagal dan return dengan kode errno
            write_status_to_file("FAIL", "REMOVE1", text);
            return -errno;
        }
        else
        {
            // Jika berhasil, log berhasil dan return 0
            write_status_to_file("SUCCESS", "REMOVE", text);
            return 0;
        }
    }
    // Pengecekan restricted
    if (strstr(path, "restricted") != NULL)
    {
        // Jika path mengandung "restricted", log gagal dan return -1
        write_status_to_file("FAIL", "REMOVE2", text);
        return -1;
    }
    else
    {
        // Jika tidak mengandung "bypass", log gagal dan return -1
        write_status_to_file("FAIL", "REMOVE3", text);
        return -1;
    }
}


//Bikin fuse dibedakan file dan directory
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr, //mendapatkan atribut file
    .readdir = xmp_readdir, 
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .create = xmp_create, 
    .rename = xmp_rename, 
    .unlink = xmp_unlink, 
    .rmdir = xmp_rmdir,  
};

void init(){
    char *url = "https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i";
    download_file(url, "./nanaxgerma.zip");
    // change_permission(HOME"/nanaxgerma.zip");
    // unzipFile("/home/naraduhita/sisop/", "/home/naraduhita/sisop/nanaxgerma.zip"); //unzip binatang.zip
    char* zipFile = "nanaxgerma.zip";
    char* unzipDir = "/home/ilham/sisopmodul4/soal2";
    unzipFile(zipFile, unzipDir);
    }

int main(int argc, char *argv[]) {
    pid_t pid; //Deklarasi sebuah variabel bertipe pid_t 

    pid = fork(); 

    if (pid<0){
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }

    init();
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);

    return 0;
}

