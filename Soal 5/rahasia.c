#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sys/wait.h>

static  const  char *dirpath = "/home/ahyun/Documents/prakmod4/rahasia";

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);
    if (res == -1) return -errno;
    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    
    else sprintf(fpath, "%s%s", dirpath, path);
    int res = 0;

    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        printf("Debug: Failed to open directory: %s\n", fpath); 
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));


        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char entry_path[5000];
        sprintf(entry_path, "%s/%s", fpath, de->d_name);

        if (stat(entry_path, &st) == -1) {
            return -errno;
        }
        
        char new_entry_path[5000];
        if (S_ISDIR(st.st_mode)) {

            if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {

                char new_name[2000];
                sprintf(new_name, "%s_E08", de->d_name);

                // Mengecek apakah kata 'E08' sudah ada dalam nama folder sebelum melakukan rename
                if (strstr(de->d_name, "E08") == NULL) {
                    // Melakukan rename folder
                    sprintf(entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);
                    if (rename(entry_path, new_entry_path) == -1) {
                        printf("Debug: Failed to rename folder: %s\n", entry_path);
                    }

                    res = xmp_readdir(entry_path, buf, filler, offset, fi);
                }
            }

        }
        //cek apakah termasuk file
        if(S_ISREG(st.st_mode)) {
            
            //apakah nama file mengandung "E08"
            if (strstr(de->d_name, "E08") == NULL) {
                char new_name[2000];
                char *ext = strrchr(de->d_name, '.');
                if (ext != NULL) {
                    sprintf(new_name, "E08_%s", de->d_name);

                    char new_entry_path[5000];
                    char old_entry_path[5000];
                    sprintf(old_entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);

                    // Memeriksa keberadaan file sebelum melakukan rename
                    if (access(new_entry_path, F_OK) == -1) {
                        // File dengan nama yang diinginkan belum ada, lanjutkan dengan proses rename
                        if (rename(entry_path, new_entry_path) == -1) {
                            printf("Debug: Failed to rename file: %s\n", entry_path);
                            perror("Error"); // Menampilkan pesan error lebih spesifik
                        }
                    } 
                }    
            }
        }

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0) break;
    }
    closedir(dp);
    return 0;
}




static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0){
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;
    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpathFrom[1000];
    char fpathTo[1000];

    sprintf(fpathFrom, "%s%s", dirpath, from);
    sprintf(fpathTo, "%s%s", dirpath, to);

    printf("rename: %s to %s\n", fpathFrom, fpathTo);

    int res = rename(fpathFrom, fpathTo);
    if (res == -1)
        return -errno;

    return 0;
}




static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};


int  main(int  argc, char *argv[])
{
    int cid;
    cid = fork();
    if(cid==0)
        system("wget -O rahasia.zip \"https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes\"");
    else if(cid>0){
        wait(NULL);
        system("unzip rahasia.zip");

        int cid2;
        cid2=fork();
        if(cid2==0){
            umask(0);
            fuse_main(argc, argv, &xmp_oper, NULL);
        }else if(cid2>0){
            wait(NULL);
            system("docker-compose up -d");
            
            /*scriptregis dan scriptext 100% chatgpt*/
            const char* scriptregis =
            "#!/bin/bash\n"
            "\n"
            "check_username_exists() {\n"
            "    local username=$1\n"
            "    grep -q \"^$username;\" credentials.txt\n"
            "}\n"
            "\n"
            "register_user() {\n"
            "    local username=$1\n"
            "    local password=$2\n"
            "\n"
            "    if check_username_exists \"$username\"; then\n"
            "        echo \"Username already exists. Please choose a different username.\"\n"
            "    else\n"
            "        hashed_password=$(echo -n \"$password\" | md5sum | awk '{print $1}')\n"
            "        echo \"$username;$hashed_password\" >> credentials.txt\n"
            "        echo \"User registered successfully.\"\n"
            "    fi\n"
            "}\n"
            "\n"
            "login_user() {\n"
            "    local username=$1\n"
            "    local password=$2\n"
            "\n"
            "    if check_username_exists \"$username\"; then\n"
            "        hashed_password=$(echo -n \"$password\" | md5sum | awk '{print $1}')\n"
            "        stored_password=$(grep \"^$username;\" credentials.txt | cut -d';' -f2)\n"
            "        if [ \"$hashed_password\" = \"$stored_password\" ]; then\n"
            "            echo \"Login successful.\"\n"
            "            docker exec -it prakmod4_no5 /bin/bash\n"
            "        else\n"
            "            echo \"Incorrect password.\"\n"
            "        fi\n"
            "    else\n"
            "        echo \"Username not found.\"\n"
            "    fi\n"
            "}\n"
            "\n"
            "while true; do\n"
            "    echo \"1. Register\"\n"
            "    echo \"2. Login\"\n"
            "    echo \"3. Exit\"\n"
            "    read -p \"Enter your choice: \" choice\n"
            "    case $choice in\n"
            "        1)\n"
            "            read -p \"Enter username: \" username\n"
            "            read -p \"Enter password: \" password\n"
            "            register_user \"$username\" \"$password\"\n"
            "            ;;\n"
            "        2)\n"
            "            read -p \"Enter username: \" username\n"
            "            read -p \"Enter password: \" password\n"
            "            login_user \"$username\" \"$password\"\n"
            "            ;;\n"
            "        3)\n"
            "            break\n"
            "            ;;\n"
            "        *)\n"
            "            echo \"Invalid choice. Please try again.\"\n"
            "            ;;\n"
            "    esac\n"
            "    echo\n"
            "done\n";

            const char *scriptext =
            "#!/bin/bash\n\n"
            "directory=\"/home/ahyun/Documents/prakmod4/rahasia\"\n\n"
            "output_file=\"extension.txt\"\n\n"
            "count_files() {\n"
            "    local search_dir=\"$1\"\n"
            "    local extensions=()\n"
            "    local dir_count=0\n\n"
            "    while IFS= read -r -d '' file; do\n"
            "        if [[ -f \"$file\" ]]; then\n"
            "            ext=\"${file##*.}\"\n"
            "            if [[ -n \"$ext\" && ! \" ${extensions[@]} \" =~ \" $ext \" ]]; then\n"
            "                extensions+=(\"$ext\")\n"
            "            fi\n"
            "        elif [[ -d \"$file\" ]]; then\n"
            "            ((dir_count++))\n"
            "        fi\n"
            "    done < <(find \"$search_dir\" -print0)\n\n"
            "    for ext in \"${extensions[@]}\"; do\n"
            "        count=$(find \"$search_dir\" -type f -iname \"*.$ext\" | wc -l)\n"
            "        echo \"$ext: $count\" >> \"$output_file\"\n"
            "    done\n\n"
            "    echo \"Directories: $dir_count\" >> \"$output_file\"\n"
            "}\n\n"
            "count_files \"$directory\"";

            system(scriptregis);
            system("tree rahasia > result.txt");

            system("touch iniskripext.sh");
            FILE* file = fopen("iniskripext.sh", "w");
            if (file) {
                fprintf(file, "%s", scriptext);
                fclose(file);
            }
            system("bash iniskripext.sh");
        }
    }
    return 0;
}
